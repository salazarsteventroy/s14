console.log("Hello Batch 144!");

/*
Javascript- we can see or log messages in our consoles

browser consoles are part of our browsers which wll
allow us see or log messages,data or information from
our programming language - Javascript.


For more browsers consoles are easily accessed through its
developer tools in the console tab
*/

console.log("troy");

/*mini activity*/

/*console.log("karekare")
console.log("karekare")
console.log("karekare")*/

let favoriteFood ="karekare";

console.log(favoriteFood);

/*
variables are a way to store information or data 
within our JS

to create a variable we first declare the name
of the variable with either let/const keyword:

let nameOfVariable

Then, we can initialize the variable with a value
or data



*/

// const keyword

/*const keyword will also allow us to create variables.
however, with a const keyword we create constant 
variables, wihch means these data to saved in a constant
will not cha,ge cannot be changed and should not be changed*/

console.log("Sample String Data");

let country = "Philippines";
let province = "Rizal";

console.log(province,country);

/*
concatentation - is a way for us to add strings together
and have a single string. We can use our "+" symbol for this.

*/

let fullAddress = province + ',' + country;

console.log(fullAddress);

let greeting = "I live in" + country;

console.log(greeting);

/*In JS, when you use the + sign with strings we have
concatenation */

let numString = "50";
let numString2 ="25";

console.log(numString + numString2);

let hero = "Captain America";

console.log(hero.length);

/*number type

		number type data can actually be used in proper
		mathematical equatons and operations

*/


let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

/*
addtion operator will allow us to add two number types
and return the sum as a number data type

operations return a value and that value can be saved
in a variable.
*/

let sum1 = num1 + num2;

console.log(sum1);

let numString3 = "100";

let sum2 = parseInt(numString3) + num1

console.log(sum2);

const name = "Jeff";
let sunriseDirection = "east"
let sunsetDirecton = "west"

console.log(name, sunriseDirection, sunsetDirecton);

console.log(num1,numString3);

let sum3 = sum1 + sum2;

let sum4 = parseInt(numString2) + num2;
console.log(sum3,sum4);

/*
subtraction operator 

will let us get the difference between two number types
it will also result to a proper number type data

when subtraction operator is used on a strng and a number, the
string will be converted into a number automatically and then JS will perform
the operaton

Ths automatic conversion from one type to another is called
type conversion or type coercion or forced coercion.
*/

let difference = num1 - num2;
console.log(difference);

console.log(numString3 - num2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";
console.log(num2 - string1);

/*multplication operator*/

let num3 = 10;
let num4 = 5;
let product = num3*num4;
console.log(product);

console.log(numString3 * num3);


/*
Division Operator
*/
let num5 = 30;
let num6 = 3;
let quotient = num5/num6;
console.log(quotient);
let quotient2 = numString3/5;
console.log(quotient2);
let quotient3 = 450/num4;
console.log(quotient3);


/*boolean (true or false)*/
/*boolean is usually used for logical operations
or for if-else conditions.
Naming convention for a variable that contans boolean is a yes or no question
*/

let isAdmin = true;
let isMarried = false;

/*Arrays*/
/*arrays are special kind of data type 
wherein we can store multiple values*/

/*an array are separated by comma, failing to do so
wll result in an error

an array is created with an array literal ([]) */

let koponanNiEugene = ["Eugene","Alfred","Dennis","Vincent"];
console.log(koponanNiEugene);

// to access an array item: arrayName[ndex]
console.log(koponanNiEugene[0]);
let array2 = ["One Punch Man",true,500,"Saitama"];
console.log(array2);

// Objects
/*
Objects are another kind of special data type used to mimic 
real world objects.
With this we can add information that makes sense, thematiically relevant
and of different data types

Each value is given a label which makes the data significant
*/

let person1 = {

	heroName: "One Punch Man",
	isRegistered: true,
	balance: 500,
	realName: "Saitama",

};

console.log(person1.realName);


let myFavoriteBands = ["paramore","mcr","turnover"];
console.log(myFavoriteBands);

let me = {

	firstName: "Troy",
	lastName: "salazar",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 20,
};

console.log(me);

/*undefined vs null*/

/*null*/
/*is the explicit declaration that there is no value*/

let sampleNull = null;

/*undefined*/
/*means that the variable exiists however a value
was not initialized with the variable*/

let undefinedSample;

console.log(undefinedSample);

/*certain processes in programming explicitly 
returns null to indicate that the task resulted to nothing*/

let foundResult = null;

/*for undefined this is normally caused by developers
creating variables that have no value or data associated with them.
the variable does exist but its value is still unknown.
*/

let person2 = {
	name: "Patricia",
	age: 28
};

// because the variable person2 doesnt exist however the property of isAdmin does not
console.log(person2.isAdmin);



// Functions


function printStar(){
	console.log("*")
	console.log("**")
	console.log("***")
}

function sayHello(name){
	console.log('hello' + name)

}

sayHello("6")


/*a function can call another function*/
function alertPrint(){
	alert('hello')
	sayHello()	
}

// function that accepts two numbers 
// and prints the sum

/*function addSum(x,y) {
	let sum = x + y
	console.log(sum)
}*/



// funtion wth 3 parameters

function printBio(lname, fname, age) {
	// console.log('hello' + lname' + 'fname +' '+ age)
		console.log(`Hello Mr.${lname} ${fname} ${age}`)
}


// return keyword
function addSum(x,y) {
	return y-x
	console.log(x+y)
}


let sumNum = addSum(3,4)

